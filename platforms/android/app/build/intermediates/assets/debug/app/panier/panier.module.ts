import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SharedModule } from "../shared/shared.module";
import { PanierRoutingModule } from "./panier-routing.module";
import { PanierComponent } from "./panier.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        PanierRoutingModule,
        SharedModule
    ],
    declarations: [
        PanierComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class PanierModule { }
