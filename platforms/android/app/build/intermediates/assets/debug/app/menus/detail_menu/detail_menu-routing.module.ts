import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { DetailMenuComponent } from "./detail_menu.component";

const routes: Routes = [
    { path: "", component:  DetailMenuComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class DetailMenuRoutingModule { }
