export interface Item {
    id: number;
    name: string;
    ville: string;
    imageSrc: string;
}