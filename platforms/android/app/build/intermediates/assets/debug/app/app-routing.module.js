"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var routes = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "home", loadChildren: "./home/home.module#HomeModule" },
    { path: "livraison", loadChildren: "./livraison/livraison.module#LivraisonModule" },
    { path: "support", loadChildren: "./support/support.module#SupportModule" },
    { path: "blog", loadChildren: "./blog/blog.module#BlogModule" },
    { path: "settings", loadChildren: "./settings/settings.module#SettingsModule" },
    { path: "mymenu", loadChildren: "./menus/my_menu/my_menu.module#MyMenuModule" },
    { path: "listmenu", loadChildren: "./menus/list_menu/list_menu.module#ListMenuModule" },
    { path: "allmenu", loadChildren: "./menus/all_menu/all_menu.module#AllMenuModule" },
    { path: "detailmenu", loadChildren: "./menus/detail_menu/detail_menu.module#DetailMenuModule" },
    { path: "login", loadChildren: "./auth/login/login.module#LoginModule" },
    { path: "register", loadChildren: "./auth/register/register.module#RegisterModule" },
    { path: "forgetpwd", loadChildren: "./auth/forgetpwd/forgetpwd.module#ForgetpwdModule" },
    { path: "panier", loadChildren: "./panier/panier.module#PanierModule" }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.NativeScriptRouterModule.forRoot(routes)],
            exports: [router_1.NativeScriptRouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLXJvdXRpbmcubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXBwLXJvdXRpbmcubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXlDO0FBRXpDLHNEQUF1RTtBQUV2RSxJQUFNLE1BQU0sR0FBVztJQUNuQixFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFO0lBQ3BELEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsK0JBQStCLEVBQUU7SUFDL0QsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSw4Q0FBOEMsRUFBRTtJQUNuRixFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLHdDQUF3QyxFQUFFO0lBQzNFLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsK0JBQStCLEVBQUU7SUFDL0QsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSwyQ0FBMkMsRUFBRTtJQUMvRSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLDZDQUE2QyxFQUFFO0lBQy9FLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsbURBQW1ELEVBQUU7SUFDdkYsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxnREFBZ0QsRUFBRTtJQUNuRixFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsWUFBWSxFQUFFLHlEQUF5RCxFQUFFO0lBQy9GLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxZQUFZLEVBQUUsdUNBQXVDLEVBQUU7SUFDeEUsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxnREFBZ0QsRUFBRTtJQUNwRixFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsWUFBWSxFQUFFLG1EQUFtRCxFQUFFO0lBQ3hGLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUscUNBQXFDLEVBQUU7Q0FDMUUsQ0FBQztBQU1GO0lBQUE7SUFBZ0MsQ0FBQztJQUFwQixnQkFBZ0I7UUFKNUIsZUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFLENBQUMsaUNBQXdCLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ25ELE9BQU8sRUFBRSxDQUFDLGlDQUF3QixDQUFDO1NBQ3RDLENBQUM7T0FDVyxnQkFBZ0IsQ0FBSTtJQUFELHVCQUFDO0NBQUEsQUFBakMsSUFBaUM7QUFBcEIsNENBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXMgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcclxuXHJcbmNvbnN0IHJvdXRlczogUm91dGVzID0gW1xyXG4gICAgeyBwYXRoOiBcIlwiLCByZWRpcmVjdFRvOiBcIi9ob21lXCIsIHBhdGhNYXRjaDogXCJmdWxsXCIgfSxcclxuICAgIHsgcGF0aDogXCJob21lXCIsIGxvYWRDaGlsZHJlbjogXCIuL2hvbWUvaG9tZS5tb2R1bGUjSG9tZU1vZHVsZVwiIH0sXHJcbiAgICB7IHBhdGg6IFwibGl2cmFpc29uXCIsIGxvYWRDaGlsZHJlbjogXCIuL2xpdnJhaXNvbi9saXZyYWlzb24ubW9kdWxlI0xpdnJhaXNvbk1vZHVsZVwiIH0sXHJcbiAgICB7IHBhdGg6IFwic3VwcG9ydFwiLCBsb2FkQ2hpbGRyZW46IFwiLi9zdXBwb3J0L3N1cHBvcnQubW9kdWxlI1N1cHBvcnRNb2R1bGVcIiB9LFxyXG4gICAgeyBwYXRoOiBcImJsb2dcIiwgbG9hZENoaWxkcmVuOiBcIi4vYmxvZy9ibG9nLm1vZHVsZSNCbG9nTW9kdWxlXCIgfSxcclxuICAgIHsgcGF0aDogXCJzZXR0aW5nc1wiLCBsb2FkQ2hpbGRyZW46IFwiLi9zZXR0aW5ncy9zZXR0aW5ncy5tb2R1bGUjU2V0dGluZ3NNb2R1bGVcIiB9LFxyXG4gICAgeyBwYXRoOiBcIm15bWVudVwiLCBsb2FkQ2hpbGRyZW46IFwiLi9tZW51cy9teV9tZW51L215X21lbnUubW9kdWxlI015TWVudU1vZHVsZVwiIH0sXHJcbiAgICB7IHBhdGg6IFwibGlzdG1lbnVcIiwgbG9hZENoaWxkcmVuOiBcIi4vbWVudXMvbGlzdF9tZW51L2xpc3RfbWVudS5tb2R1bGUjTGlzdE1lbnVNb2R1bGVcIiB9LFxyXG4gICAgeyBwYXRoOiBcImFsbG1lbnVcIiwgbG9hZENoaWxkcmVuOiBcIi4vbWVudXMvYWxsX21lbnUvYWxsX21lbnUubW9kdWxlI0FsbE1lbnVNb2R1bGVcIiB9LFxyXG4gICAgeyBwYXRoOiBcImRldGFpbG1lbnVcIiwgbG9hZENoaWxkcmVuOiBcIi4vbWVudXMvZGV0YWlsX21lbnUvZGV0YWlsX21lbnUubW9kdWxlI0RldGFpbE1lbnVNb2R1bGVcIiB9LFxyXG4gICAgeyBwYXRoOiBcImxvZ2luXCIsIGxvYWRDaGlsZHJlbjogXCIuL2F1dGgvbG9naW4vbG9naW4ubW9kdWxlI0xvZ2luTW9kdWxlXCIgfSxcclxuICAgIHsgcGF0aDogXCJyZWdpc3RlclwiLCBsb2FkQ2hpbGRyZW46IFwiLi9hdXRoL3JlZ2lzdGVyL3JlZ2lzdGVyLm1vZHVsZSNSZWdpc3Rlck1vZHVsZVwiIH0sXHJcbiAgICB7IHBhdGg6IFwiZm9yZ2V0cHdkXCIsIGxvYWRDaGlsZHJlbjogXCIuL2F1dGgvZm9yZ2V0cHdkL2ZvcmdldHB3ZC5tb2R1bGUjRm9yZ2V0cHdkTW9kdWxlXCIgfSxcclxuICAgIHsgcGF0aDogXCJwYW5pZXJcIiwgbG9hZENoaWxkcmVuOiBcIi4vcGFuaWVyL3Bhbmllci5tb2R1bGUjUGFuaWVyTW9kdWxlXCIgfVxyXG5dO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGUuZm9yUm9vdChyb3V0ZXMpXSxcclxuICAgIGV4cG9ydHM6IFtOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGVdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBcHBSb3V0aW5nTW9kdWxlIHsgfVxyXG4iXX0=