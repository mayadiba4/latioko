import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

const routes: Routes = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "home", loadChildren: "./home/home.module#HomeModule" },
    { path: "livraison", loadChildren: "./livraison/livraison.module#LivraisonModule" },
    { path: "support", loadChildren: "./support/support.module#SupportModule" },
    { path: "blog", loadChildren: "./blog/blog.module#BlogModule" },
    { path: "settings", loadChildren: "./settings/settings.module#SettingsModule" },
    { path: "mymenu", loadChildren: "./menus/my_menu/my_menu.module#MyMenuModule" },
    { path: "listmenu", loadChildren: "./menus/list_menu/list_menu.module#ListMenuModule" },
    { path: "allmenu", loadChildren: "./menus/all_menu/all_menu.module#AllMenuModule" },
    { path: "detailmenu", loadChildren: "./menus/detail_menu/detail_menu.module#DetailMenuModule" },
    { path: "login", loadChildren: "./auth/login/login.module#LoginModule" },
    { path: "register", loadChildren: "./auth/register/register.module#RegisterModule" },
    { path: "forgetpwd", loadChildren: "./auth/forgetpwd/forgetpwd.module#ForgetpwdModule" },
    { path: "panier", loadChildren: "./panier/panier.module#PanierModule" }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
