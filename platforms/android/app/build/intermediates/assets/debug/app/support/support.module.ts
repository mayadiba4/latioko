import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SharedModule } from "../shared/shared.module";
import { SupportRoutingModule } from "./support-routing.module";
import { SupportComponent } from "./support.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        SupportRoutingModule,
        SharedModule
    ],
    declarations: [
        SupportComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SupportModule { }
