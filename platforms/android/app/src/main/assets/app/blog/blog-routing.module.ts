import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { ItemService } from "./blog.service";
import { BlogComponent } from "./blog.component";

const routes: Routes = [
    { path: "", component: BlogComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class BlogRoutingModule { }
