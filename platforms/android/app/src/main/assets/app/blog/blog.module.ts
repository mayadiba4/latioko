import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SharedModule } from "../shared/shared.module";
import { ItemService } from "./blog.service";
import { BlogRoutingModule } from "./blog-routing.module";
import { BlogComponent } from "./blog.component";


@NgModule({
    imports: [
        NativeScriptCommonModule,
        BlogRoutingModule,
        SharedModule
    ],
    declarations: [
        BlogComponent
    ],
    
    providers: [
        ItemService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class BlogModule { }
