import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SharedModule } from "../../../shared/shared.module";
import { BaseRoutingModule } from "./base-routing.module";
import { BaseComponent } from "./base.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        BaseRoutingModule,
        SharedModule
    ],
    declarations: [
        BaseComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class BaseModule { }
