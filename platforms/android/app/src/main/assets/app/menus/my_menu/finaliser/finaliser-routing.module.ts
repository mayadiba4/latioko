import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { FinaliserComponent } from "./finaliser.component";

const routes: Routes = [
    { path: "", component: FinaliserComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class FinaliserRoutingModule { }
