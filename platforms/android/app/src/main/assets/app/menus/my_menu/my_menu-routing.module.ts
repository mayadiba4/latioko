import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { MyMenuComponent } from "./my_menu.component";
import { BaseComponent } from "./base/base.component";

const routes: Routes = [
    { path: "", component: MyMenuComponent, children: [
        { path: 'base', loadChildren: "./menus/my_menu/base/base.module#BaseModule", outlet: 'catoutlet'},
        
       /* { path: 'cats/:name', component: CatDetailsComponent, outlet: 'catoutlet'},
        { path: 'dogs', component: DogsComponent, outlet: 'dogoutlet'},
        { path: 'dogs/:id', component: DogDetailsComponent, outlet: 'dogoutlet'}*/
      ] }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class MyMenuRoutingModule { }
