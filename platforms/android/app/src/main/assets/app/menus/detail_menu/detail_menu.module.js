"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("nativescript-angular/common");
var shared_module_1 = require("../../shared/shared.module");
var detail_menu_routing_module_1 = require("./detail_menu-routing.module");
var detail_menu_component_1 = require("./detail_menu.component");
var DetailMenuModule = /** @class */ (function () {
    function DetailMenuModule() {
    }
    DetailMenuModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.NativeScriptCommonModule,
                detail_menu_routing_module_1.DetailMenuRoutingModule,
                shared_module_1.SharedModule
            ],
            declarations: [
                detail_menu_component_1.DetailMenuComponent
            ],
            schemas: [
                core_1.NO_ERRORS_SCHEMA
            ]
        })
    ], DetailMenuModule);
    return DetailMenuModule;
}());
exports.DetailMenuModule = DetailMenuModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGV0YWlsX21lbnUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZGV0YWlsX21lbnUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJEO0FBQzNELHNEQUF1RTtBQUV2RSw0REFBMEQ7QUFDMUQsMkVBQXdFO0FBQ3hFLGlFQUErRDtBQWUvRDtJQUFBO0lBQWlDLENBQUM7SUFBcEIsZ0JBQWdCO1FBYjdCLGVBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRTtnQkFDTCxpQ0FBd0I7Z0JBQ3hCLG9EQUF1QjtnQkFDdkIsNEJBQVk7YUFDZjtZQUNELFlBQVksRUFBRTtnQkFDViwyQ0FBbUI7YUFDdEI7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wsdUJBQWdCO2FBQ25CO1NBQ0osQ0FBQztPQUNZLGdCQUFnQixDQUFJO0lBQUQsdUJBQUM7Q0FBQSxBQUFsQyxJQUFrQztBQUFwQiw0Q0FBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgTk9fRVJST1JTX1NDSEVNQSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdENvbW1vbk1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9jb21tb25cIjtcclxuXHJcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gXCIuLi8uLi9zaGFyZWQvc2hhcmVkLm1vZHVsZVwiO1xyXG5pbXBvcnQgeyAgRGV0YWlsTWVudVJvdXRpbmdNb2R1bGUgfSBmcm9tIFwiLi9kZXRhaWxfbWVudS1yb3V0aW5nLm1vZHVsZVwiO1xyXG5pbXBvcnQgeyAgRGV0YWlsTWVudUNvbXBvbmVudCB9IGZyb20gXCIuL2RldGFpbF9tZW51LmNvbXBvbmVudFwiO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBOYXRpdmVTY3JpcHRDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgRGV0YWlsTWVudVJvdXRpbmdNb2R1bGUsXHJcbiAgICAgICAgU2hhcmVkTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgRGV0YWlsTWVudUNvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIHNjaGVtYXM6IFtcclxuICAgICAgICBOT19FUlJPUlNfU0NIRU1BXHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyAgRGV0YWlsTWVudU1vZHVsZSB7IH1cclxuIl19