import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SharedModule } from "../../shared/shared.module";
import {  DetailMenuRoutingModule } from "./detail_menu-routing.module";
import {  DetailMenuComponent } from "./detail_menu.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        DetailMenuRoutingModule,
        SharedModule
    ],
    declarations: [
        DetailMenuComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class  DetailMenuModule { }
