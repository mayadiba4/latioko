import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { AllMenuComponent } from "./all_menu.component";

const routes: Routes = [
    { path: "", component: AllMenuComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class AllMenuRoutingModule { }
