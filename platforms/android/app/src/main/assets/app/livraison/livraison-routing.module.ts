import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { LivraisonComponent } from "./livraison.component";

const routes: Routes = [
    { path: "", component: LivraisonComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class LivraisonRoutingModule { }
