import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SharedModule } from "../shared/shared.module";
import { LivraisonRoutingModule } from "./livraison-routing.module";
import { LivraisonComponent } from "./livraison.component";
import { TNSCheckBoxModule } from 'nativescript-checkbox/angular';

@NgModule({
    imports: [
        NativeScriptCommonModule,
        LivraisonRoutingModule,
        SharedModule,
        TNSCheckBoxModule
    ],
    declarations: [
        LivraisonComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class LivraisonModule { }
