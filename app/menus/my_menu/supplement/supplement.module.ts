import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SharedModule } from "../../../shared/shared.module";
import { SupplementRoutingModule } from "./supplement-routing.module";
import { SupplementComponent } from "./supplement.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        SupplementRoutingModule,
        SharedModule
    ],
    declarations: [
        SupplementComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SupplementModule { }
