import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SharedModule } from "../../../shared/shared.module";
import { FinaliserRoutingModule } from "./finaliser-routing.module";
import { FinaliserComponent } from "./finaliser.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        FinaliserRoutingModule,
        SharedModule
    ],
    declarations: [
        FinaliserComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class FinaliserModule { }
