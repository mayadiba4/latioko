import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SharedModule } from "../../../shared/shared.module";
import { SauceRoutingModule } from "./sauce-routing.module";
import { SauceComponent } from "./sauce.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        SauceRoutingModule,
        SharedModule
    ],
    declarations: [
        SauceComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SauceModule { }
