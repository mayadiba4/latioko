import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SharedModule } from "../../../shared/shared.module";
import { DessertsRoutingModule } from "./desserts-routing.module";
import { DessertsComponent } from "./desserts.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        DessertsRoutingModule,
        SharedModule
    ],
    declarations: [
        DessertsComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class DessertsModule { }
