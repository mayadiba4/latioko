import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SharedModule } from "../../shared/shared.module";
import { MyMenuRoutingModule } from "./my_menu-routing.module";
import { MyMenuComponent } from "./my_menu.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        MyMenuRoutingModule,
        SharedModule
    ],
    declarations: [
        MyMenuComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class MyMenuModule { }
