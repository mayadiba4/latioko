import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SharedModule } from "../../../shared/shared.module";
import { ToppingsRoutingModule } from "./toppings-routing.module";
import { ToppingsComponent } from "./toppings.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        ToppingsRoutingModule,
        SharedModule
    ],
    declarations: [
        ToppingsComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ToppingsModule { }
