import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SharedModule } from "../../../shared/shared.module";
import { BoissonsRoutingModule } from "./boissons-routing.module";
import { BoissonsComponent } from "./boissons.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        BoissonsRoutingModule,
        SharedModule
    ],
    declarations: [
        BoissonsComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class BoissonsModule { }
