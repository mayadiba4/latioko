import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SharedModule } from "../../shared/shared.module";
import { AllMenuRoutingModule } from "./all_menu-routing.module";
import { AllMenuComponent } from "./all_menu.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        AllMenuRoutingModule,
        SharedModule
    ],
    declarations: [
        AllMenuComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AllMenuModule { }
