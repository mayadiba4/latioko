import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SharedModule } from "../../shared/shared.module";
import { ListMenuRoutingModule } from "./list_menu-routing.module";
import { ListMenuComponent } from "./list_menu.component";


@NgModule({
    imports: [
        NativeScriptCommonModule,
        ListMenuRoutingModule,
        SharedModule,
        
    ],
    declarations: [
        ListMenuComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ListMenuModule { }
