import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SharedModule } from "../../shared/shared.module";
import { ForgetpwdRoutingModule } from "./forgetpwd-routing.module";
import { ForgetpwdComponent } from "./forgetpwd.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        ForgetpwdRoutingModule,
        SharedModule
    ],
    declarations: [
        ForgetpwdComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ForgetpwdModule { }
