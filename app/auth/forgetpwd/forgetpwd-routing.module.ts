import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { ForgetpwdComponent } from "./forgetpwd.component";

const routes: Routes = [
    { path: "", component: ForgetpwdComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ForgetpwdRoutingModule { }
