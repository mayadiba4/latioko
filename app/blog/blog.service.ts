import { Injectable } from "@angular/core";

import { Item } from "./blog";


@Injectable()
export class ItemService {
    private items = new Array<Item>(
        { id:1, name: "COMMENT RÉDUIRE LA CARENCE EN VITAMINE B12 DES RÉGIMES VÉGÉTARIENS ET VÉGÉTALIENS ?", ville: 'Dakar Plateau', imageSrc: "~/images/person.jpeg" },
        { id: 2, name: "COMMENT RÉDUIRE LA CARENCE EN VITAMINE B12 DES RÉGIMES VÉGÉTARIENS ET VÉGÉTALIENS ?", ville: 'Liberté1', imageSrc: "~/images/person.jpeg" },
        { id: 3, name: "COMMENT RÉDUIRE LA CARENCE EN VITAMINE B12 DES RÉGIMES VÉGÉTARIENS ET VÉGÉTALIENS ?", ville: 'Grand yoff', imageSrc: "~/images/person.jpeg" },
        { id: 4, name: "COMMENT RÉDUIRE LA CARENCE EN VITAMINE B12 DES RÉGIMES VÉGÉTARIENS ET VÉGÉTALIENS ?", ville: 'sacre coeur', imageSrc: "~/images/person.jpeg" },
        { id: 5,name: "COMMENT RÉDUIRE LA CARENCE EN VITAMINE B12 DES RÉGIMES VÉGÉTARIENS ET VÉGÉTALIENS ?", ville: 'HLM', imageSrc: "~/images/person.jpeg" },
        { id: 6,name: "COMMENT RÉDUIRE LA CARENCE EN VITAMINE B12 DES RÉGIMES VÉGÉTARIENS ET VÉGÉTALIENS ?", ville: 'Ouakam', imageSrc: "~/images/person.jpeg" },
        { id: 7, name: "COMMENT RÉDUIRE LA CARENCE EN VITAMINE B12 DES RÉGIMES VÉGÉTARIENS ET VÉGÉTALIENS ?", ville: 'Bourguiba', imageSrc: "~/images/person.jpeg" },
        { id: 8,name: "COMMENT RÉDUIRE LA CARENCE EN VITAMINE B12 DES RÉGIMES VÉGÉTARIENS ET VÉGÉTALIENS ?", ville: 'Derklé', imageSrc: "~/images/person.jpeg" },
        { id: 9, name: "COMMENT RÉDUIRE LA CARENCE EN VITAMINE B12 DES RÉGIMES VÉGÉTARIENS ET VÉGÉTALIENS ?", ville: 'Pikine', imageSrc: "~/images/person.jpeg" },
        { id: 10,name: "COMMENT RÉDUIRE LA CARENCE EN VITAMINE B12 DES RÉGIMES VÉGÉTARIENS ET VÉGÉTALIENS ?", ville: 'Guediawaye', imageSrc: "~/images/person.jpeg" },
        { id: 11, name: "COMMENT RÉDUIRE LA CARENCE EN VITAMINE B12 DES RÉGIMES VÉGÉTARIENS ET VÉGÉTALIENS ?", ville: 'HAMO', imageSrc: "~/images/person.jpeg" },
        { id: 12, name: "COMMENT RÉDUIRE LA CARENCE EN VITAMINE B12 DES RÉGIMES VÉGÉTARIENS ET VÉGÉTALIENS ?", ville: 'Golf', imageSrc: "~/images/person.jpeg" },
        { id: 13,name: "COMMENT RÉDUIRE LA CARENCE EN VITAMINE B12 DES RÉGIMES VÉGÉTARIENS ET VÉGÉTALIENS ?", ville: 'Dakar', imageSrc: "~/images/person.jpeg" },
        { id: 14,name: "COMMENT RÉDUIRE LA CARENCE EN VITAMINE B12 DES RÉGIMES VÉGÉTARIENS ET VÉGÉTALIENS ?", ville: 'Corniche', imageSrc: "~/images/person.jpeg" },
        { id: 15, name: "COMMENT RÉDUIRE LA CARENCE EN VITAMINE B12 DES RÉGIMES VÉGÉTARIENS ET VÉGÉTALIENS ?", ville: 'UCAD', imageSrc: "~/images/person.jpeg" },
        { id: 16,name: "COMMENT RÉDUIRE LA CARENCE EN VITAMINE B12 DES RÉGIMES VÉGÉTARIENS ET VÉGÉTALIENS ?", ville: 'MBAO', imageSrc: "~/images/person.jpeg" }
    );

    getItems(): Item[] {
        return this.items;
    }

    getItem(id: number): Item {
        return this.items.filter(item => item.id === id)[0];
    }
}
