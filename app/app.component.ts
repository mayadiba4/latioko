import { Component } from "@angular/core";
import { Page } from "tns-core-modules/ui/page/page";

@Component({
    selector: "ns-app",
    templateUrl: "app.component.html"
})
export class AppComponent { 
    constructor(public _page: Page) {
       this._page.actionBarHidden = true;
    }
}
